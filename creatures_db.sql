-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema creatures_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema creatures_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `creatures_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `creatures_db` ;

-- -----------------------------------------------------
-- Table `creatures_db`.`admins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatures_db`.`admins` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Искусственный ПК.',
  `login` VARCHAR(10) NOT NULL COMMENT 'Логин администратора.',
  `password` CHAR(64) NOT NULL COMMENT 'Пароль администратора.',
  `avatar` MEDIUMBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `creatures_db`.`creatures`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatures_db`.`creatures` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Искусственный ПК.',
  `name` VARCHAR(20) NOT NULL COMMENT 'Имя существа.',
  `limb_quantity` INT(11) NOT NULL COMMENT 'Количество конечностей существа.',
  `head_quantity` INT(11) NOT NULL COMMENT 'Количество голов существа.',
  `eye_quantity` INT(11) NOT NULL COMMENT 'Количество глаз существа.',
  `gender` ENUM('MALE', 'FEMALE', 'NONE') NOT NULL COMMENT 'Пол существа.',
  `description` TEXT NOT NULL COMMENT 'Описание существа.',
  `creator_id` INT(11) UNSIGNED NOT NULL COMMENT 'ID админа, который создал существо.',
  `rating` DECIMAL(3,2) NULL DEFAULT NULL COMMENT 'Оценка существа. Изначально пустой. Заполняется(очищается) триггерами в таблице marks.',
  `image` MEDIUMBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `fk_admins_creatures_idx` (`creator_id` ASC),
  CONSTRAINT `fk_admins_creatures`
    FOREIGN KEY (`creator_id`)
    REFERENCES `creatures_db`.`admins` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `creatures_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatures_db`.`users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Искусственный ПК.',
  `login` VARCHAR(10) NOT NULL COMMENT 'Логин пользователя.',
  `password` CHAR(64) NOT NULL COMMENT 'Пароль пользователя.',
  `status` DECIMAL(4,2) NULL DEFAULT '5.00' COMMENT 'Статус пользователя.',
  `is_banned` TINYINT(4) NULL DEFAULT '0' COMMENT 'Забанен ли пользователь.',
  `avatar` MEDIUMBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `creatures_db`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatures_db`.`comments` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Искусственный ПК.',
  `comment_content` TEXT NOT NULL COMMENT 'Текст комментария.',
  `creature_id` INT(11) UNSIGNED NOT NULL COMMENT 'ID существа, на которого оставлен комментарий.',
  `user_id` INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'ID пользователя, который оставил комментарий.',
  PRIMARY KEY (`id`),
  INDEX `fk_creature_id_idx` (`creature_id` ASC),
  INDEX `fk_comments_user_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_creatures_comments`
    FOREIGN KEY (`creature_id`)
    REFERENCES `creatures_db`.`creatures` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_comments`
    FOREIGN KEY (`user_id`)
    REFERENCES `creatures_db`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `creatures_db`.`marks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatures_db`.`marks` (
  `mark_value` DECIMAL(3,2) NOT NULL COMMENT 'Значение оценки.',
  `creature_id` INT(11) UNSIGNED NOT NULL COMMENT 'ID существа, которому принадлежит оценка.',
  `user_id` INT(11) UNSIGNED NOT NULL COMMENT 'ID пользователя, который поставил оценку.',
  `status_component` DOUBLE NOT NULL DEFAULT '5',
  PRIMARY KEY (`creature_id`, `user_id`),
  INDEX `fk_marks_creatures1_idx` (`creature_id` ASC),
  INDEX `fk_users_marks_idx` (`user_id` ASC),
  CONSTRAINT `fk_creatures_marks`
    FOREIGN KEY (`creature_id`)
    REFERENCES `creatures_db`.`creatures` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_marks`
    FOREIGN KEY (`user_id`)
    REFERENCES `creatures_db`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;

USE `creatures_db`;

DELIMITER $$
USE `creatures_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `creatures_db`.`marks_AFTER_DELETE`
AFTER DELETE ON `creatures_db`.`marks`
FOR EACH ROW
BEGIN
					UPDATE creatures 
						SET creatures.rating=
							(SELECT AVG(marks.mark_value) 
							FROM marks 
							WHERE marks.creature_id = old.creature_id) 
                        WHERE creatures.id = old.creature_id;
					UPDATE users
						SET users.status=
							(SELECT AVG(marks.status_component)
							FROM marks
                            WHERE marks.user_id = old.user_id)
						WHERE users.id = old.user_id;
END$$

USE `creatures_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `creatures_db`.`marks_AFTER_INSERT`
AFTER INSERT ON `creatures_db`.`marks`
FOR EACH ROW
BEGIN
					UPDATE creatures 
						SET creatures.rating=
							(SELECT AVG(marks.mark_value) 
							FROM marks 
							WHERE marks.creature_id = new.creature_id) 
                        WHERE creatures.id = new.creature_id;
					UPDATE users
						SET users.status=
							(SELECT AVG(marks.status_component)
							FROM marks
                            WHERE marks.user_id = new.user_id)
						WHERE users.id = new.user_id;
END$$

USE `creatures_db`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `creatures_db`.`marks_AFTER_UPDATE`
AFTER UPDATE ON `creatures_db`.`marks`
FOR EACH ROW
BEGIN
					UPDATE creatures 
						SET creatures.rating=
							(SELECT AVG(marks.mark_value) 
							FROM marks 
							WHERE marks.creature_id = new.creature_id) 
                        WHERE creatures.id = new.creature_id;
					UPDATE users
						SET users.status=
							(SELECT AVG(marks.status_component)
							FROM marks
                            WHERE marks.user_id = new.user_id)
						WHERE users.id = new.user_id;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
