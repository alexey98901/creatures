package com.epam.creatures.dao;

import com.epam.creatures.entity.Mark;

import java.util.List;

/**
 * The interface Mark table dao.
 */
public interface MarkTableDao {

    /**
     * Find marks list.
     *
     * @param userId the user id
     * @return the list
     * @throws DaoException the dao exception
     */
    List<Mark> findMarksByUserId(Integer userId) throws DaoException;

}
