package com.epam.creatures.dao;

import java.io.InputStream;

/**
 * The interface Creature table dao.
 */
public interface CreatureTableDao {

    /**
     * Update creature image boolean.
     *
     * @param id    the id
     * @param image the image
     * @return the boolean
     * @throws DaoException the dao exception
     */
    boolean updateCreatureImage(Integer id, InputStream image) throws DaoException;
}
