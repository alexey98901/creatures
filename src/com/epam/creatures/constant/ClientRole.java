package com.epam.creatures.constant;

/**
 * The enum Client role.
 */
public enum ClientRole {
    /**
     * User client role.
     */
    USER,
    /**
     * Admin client role.
     */
    ADMIN
}
