package com.epam.creatures.constant;

/**
 * The type Mark column.
 */
public class MarkColumn {
    /**
     * The constant MARK_VALUE.
     */
    public static final String MARK_VALUE = "mark_value";
    /**
     * The constant CREATURE_ID.
     */
    public static final String CREATURE_ID = "creature_id";
    /**
     * The constant USER_ID.
     */
    public static final String USER_ID = "user_id";
    /**
     * The constant STATUS_COMPONENT.
     */
    public static final String STATUS_COMPONENT = "status_component";

    private MarkColumn(){}
}
