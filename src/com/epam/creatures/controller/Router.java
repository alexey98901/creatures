package com.epam.creatures.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * The type Router.
 */
public class Router {

    /**
     * The enum Route type.
     */
    public enum RouteType{
        /**
         * Forward route type.
         */
        FORWARD,
        /**
         * Redirect route type.
         */
        REDIRECT
    }

    private RouteType routeType;
    private String route;

    /**
     * Instantiates a new Router.
     *
     * @param routeType the route type
     * @param route     the route
     */
    public Router(RouteType routeType, String route) {
        this.routeType = routeType;
        this.route = route;
    }

    /**
     * Gets route type.
     *
     * @return the route type
     */
    public RouteType getRouteType() {
        return routeType;
    }

    /**
     * Sets route type.
     *
     * @param routeType the route type
     */
    public void setRouteType(RouteType routeType) {
        this.routeType = routeType;
    }

    /**
     * Gets route.
     *
     * @return the route
     */
    public String getRoute() {
        return route;
    }

    /**
     * Sets route.
     *
     * @param route the route
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Send.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException      the io exception
     * @throws ServletException the servlet exception
     */
    void send(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        if (routeType == Router.RouteType.FORWARD) {
            request.getRequestDispatcher(route).forward(request, response);

        } else if (routeType == Router.RouteType.REDIRECT) {
            response.sendRedirect(route);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Router router = (Router) o;
        return routeType == router.routeType &&
                Objects.equals(route, router.route);
    }

    @Override
    public int hashCode() {

        return Objects.hash(routeType, route);
    }

    @Override
    public String toString() {
        return "Router{" +
                "routeType=" + routeType +
                ", route='" + route + '\'' +
                '}';
    }
}
