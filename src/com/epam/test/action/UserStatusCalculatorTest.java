package com.epam.test.action;

import com.epam.creatures.action.UserStatusCalculator;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * The type User status calculator test.
 */
public class UserStatusCalculatorTest {
    private UserStatusCalculator statusCalculator = new UserStatusCalculator();

    /**
     * Test calculate status 1.
     */
    @Test
    public void testCalculateStatus1(){
        Double expected = 10d;
        Double actual = statusCalculator.calculateStatus(5d,5d);

        Assert.assertEquals(actual,expected);
    }

    /**
     * Test calculate status 2.
     */
    @Test
    public void testCalculateStatus2(){
        Double expected = 0d;
        Double actual = statusCalculator.calculateStatus(0d,5d);

        Assert.assertEquals(actual,expected);
    }

    /**
     * Test calculate status 3.
     */
    @Test
    public void testCalculateStatus3(){

        Double expected = 5d;
        Double actual = statusCalculator.calculateStatus(2.5,5d);

        Assert.assertEquals(actual,expected);
    }

    /**
     * Test calculate status 4.
     */
    @Test
    public void testCalculateStatus4(){

        Double expected = 0d;
        Double actual = statusCalculator.calculateStatus(0d,0d);

        Assert.assertNotEquals(actual,expected);
    }
}
